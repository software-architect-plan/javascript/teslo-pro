import { ApiProperty } from "@nestjs/swagger";
import { Type } from "class-transformer";
import { IsOptional, IsPositive, Min } from "class-validator";


export class PaginationDto {

    @ApiProperty({
        description: 'how many rows do you need',
        default: 10,
    })
    @IsOptional()
    @Type( () => Number) // enableImplicitConversions: true
    limit?: number;

    @ApiProperty({
        description: 'how many rows do you want to skip',
        default: 0,
    })
    @IsOptional()
    @Min(0)
    @Type( () => Number) // enableImplicitConversions: true
    offset?: number;
}